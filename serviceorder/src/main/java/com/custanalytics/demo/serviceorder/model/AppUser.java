package com.custanalytics.demo.serviceorder.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table(name = "ACCOUNT")
public class AppUser {
	
	@Id
	@Column(name = "ACCOUNT_ID", unique = true, 
		nullable = false)
	Integer accountId;
	@NotEmpty(message="Please enter an email address")
	@Column(name = "EMAIL", unique = true)
	String email;
	@NotEmpty(message = "Please enter your password.")
	@Column(name = "PASSWORD", unique = true)
	String password;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

}
