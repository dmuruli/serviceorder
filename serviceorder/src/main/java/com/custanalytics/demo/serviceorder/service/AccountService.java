package com.custanalytics.demo.serviceorder.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.custanalytics.demo.serviceorder.model.AppUser;
import com.custanalytics.demo.serviceorder.repository.AccountDao;

@Service("accountService")
public class AccountService {

	@Autowired
	private AccountDao accountRepository;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AccountService.class);
	/*
	 * Method returns a user given a user name and password, if the user does
	 * not exist the method returns null;
	 */
	public AppUser login(String userName, String password) {
		AppUser user = null;
		user=accountRepository.login(userName, password);
		return user;
	}
	/**
	 * Method creates a user, given user details. If the system is not able to
	 * create the user the method returns null;
	 * @param user
	 * @return
	 */
	public AppUser createUser(AppUser user) {
		AppUser createdUser = null;
		return createdUser;
	}

	public AppUser getUserById(Long id) {
		AppUser user = null;
		user =accountRepository.findOne(id);
		return user;
	}
}
