package com.custanalytics.demo.serviceorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.custanalytics.demo.serviceorder.model.AppUser;

public interface AccountDao  extends JpaRepository<AppUser, Long>{
	
	@Query("SELECT a FROM AppUser a where a.email = ?1 AND a.password = ?2")
	public AppUser login(String userName, String password);
	

	
	
}
