package com.custanalytics.demo.serviceorder.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESS")
public class Address {
	
	
	/**
	 *  `ADDRESS_ID` bigint NOT NULL ,
  `CITY` varchar(150) NOT NULL,
  `STATE_PROVINCE` varchar(150) NOT NULL,
  `COUNTRY` varchar(150) NOT NULL,
  `ADDRESS_LINE_1` varchar(150) NOT NULL,
  `ADDRESS_LINE_2` varchar(150) NOT NULL,
  `LOCATION_PHONE` bigint NOT NULL ,
  `NOTES` varchar(150) NOT NULL,
	 */
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADDRESS_ID")
    private Integer addressId;
	@Column(name = "ADDRESS_LINE_1")
	private String addressLine1;
	@Column(name = "ADDRESS_LINE_2")
	private String addressLine2;
	@Column(name = "CITY")
	private String city;
	@Column(name = "STATE_PROVINCE")
	private String stateOrProvince;
	@Column(name = "COUNTRY")
	private String country;
	@Column(name = "POSTAL_CODE")
	private String postalCode;
	@Column(name = "LOCATION_PHONE")
	private String locationPhone;
	/*@OneToOne(cascade=CascadeType.ALL)  
    @JoinTable(name="CUSTOMER_ADDRESS",  
    joinColumns={@JoinColumn(name="ADDRESS_ID", referencedColumnName="ADDRESS_ID")},  
    inverseJoinColumns={@JoinColumn(name="CUSTOMER_ID", referencedColumnName="CUSTOMER_ID")}) */ 
	//private Customer customer;
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateOrProvince() {
		return stateOrProvince;
	}
	public void setStateOrProvince(String stateOrProvince) {
		this.stateOrProvince = stateOrProvince;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getLocationPhone() {
		return locationPhone;
	}
	public void setLocationPhone(String locationPhone) {
		this.locationPhone = locationPhone;
	}
	

	

}
