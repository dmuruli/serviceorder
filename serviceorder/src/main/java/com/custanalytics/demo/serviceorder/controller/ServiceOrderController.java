package com.custanalytics.demo.serviceorder.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.custanalytics.demo.serviceorder.model.AppUser;
import com.custanalytics.demo.serviceorder.model.Customer;
import com.custanalytics.demo.serviceorder.model.ServiceOrder;
import com.custanalytics.demo.serviceorder.service.CustomerService;
@Controller
public class ServiceOrderController {
	private static final Logger logger = Logger.getLogger(ServiceOrderController.class);
	
	@Autowired
	CustomerService customerService;
	@RequestMapping(value="/createserviceorder")
	public ModelAndView initLogin(){
		logger.info("Creating Service Order");
		ModelAndView mav = new ModelAndView("serviceorder");
		AppUser appUser  =new AppUser();
		//load list of customers for technician
		
		mav.addObject("user", appUser);
		//Create dummy list of customers
		List<Customer>customers =customerService.getlAllCustomers();
		customers=customerService.dummyCustomers();
		ServiceOrder serviceOrder = new ServiceOrder();
		mav.addObject("serviceOrder", serviceOrder);
		mav.addObject("customers", customers);
		return mav;
	}
	
	public ModelAndView createServiceOrder(@ModelAttribute("serviceOrder")ServiceOrder serviceOrder ){
		ModelAndView mav = new ModelAndView();
		logger.info("");
		
		return mav;
	}
}
