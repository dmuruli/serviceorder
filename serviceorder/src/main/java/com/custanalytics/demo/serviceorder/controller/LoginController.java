package com.custanalytics.demo.serviceorder.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.custanalytics.demo.serviceorder.model.AppUser;
import com.custanalytics.demo.serviceorder.service.AccountService;

@Controller
@SessionAttributes("user")
public class LoginController {
	@Autowired
	AccountService accountService;
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	@RequestMapping(value="/home")
	public ModelAndView initLogin(){
		logger.info("Init Login");
		ModelAndView mav = new ModelAndView("login");
		AppUser appUser  =new AppUser();
		mav.addObject("user", appUser);
		return mav;
	}
	@RequestMapping(value="/login")
	public ModelAndView login(@ModelAttribute("user") @Valid AppUser user,
			BindingResult result){
	  logger.info("The user email is: " + user.getEmail() +" the password is : " + user.getPassword());
	  ModelAndView mav = new ModelAndView("login");
	  mav.addObject("user", user);
	  if (result.hasErrors()){
		  logger.error("Errors from screen");
	
		  mav.addAllObjects(result.getModel());
		  logger.info("Validation failed, email/password not valid");
	
		  String pageError ="The login email " + user .getEmail() +"does not exist in the system, "
		  		+" contact "
		  		+ "Administrator(dsmuruli@yahoo.com) if you "
		  		+ "do not have a valid login";

		  return mav; 
	  }else {
		AppUser userAccount=  accountService.login(user.getEmail(), user.getPassword());
		if(userAccount==null){
			String pageError ="The login email " + user .getEmail() +"does not exist in the system, "
			  		+" contact "
			  		+ "Administrator(dsmuruli@yahoo.com) if you "
			  		+ "do not have a valid login";
		logger.error("User not found in system");

		result.reject("login.invalid", pageError);
		mav.setViewName("login");
		return mav ;
		}else{
			logger.info("User was found.");
		}		 
	  }
	  String successview ="redirect:/createserviceorder";
	   mav = new ModelAndView(successview);

	  return mav;
	}
}
