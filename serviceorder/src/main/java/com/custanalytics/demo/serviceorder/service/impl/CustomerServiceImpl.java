package com.custanalytics.demo.serviceorder.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.custanalytics.demo.serviceorder.model.Customer;
import com.custanalytics.demo.serviceorder.repository.AccountDao;
import com.custanalytics.demo.serviceorder.repository.CustomerRepository;
import com.custanalytics.demo.serviceorder.service.AccountService;
import com.custanalytics.demo.serviceorder.service.CustomerService;
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CustomerServiceImpl.class);
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Customer> getlAllCustomers() {
		List<Customer> customerList = new ArrayList<Customer>();
		customerList =customerRepository.findAll();
		return customerList;
	}

	@Override
	public Customer getCustomer(Long customerId) {
		Customer customer = null;
		customer =customerRepository.getOne(customerId);
		return customer;
	}

	@Override
	public List<Customer> findAssignedCustomers(Long technicianId) {
		List<Customer> customerList = new ArrayList<Customer>();
		return customerList;
	}

	@Override
	@Transactional
	public void saveCustomer(Customer customer) {
		customerRepository.save(customer);	
	}

	@Override
	public List<Customer> updateCustomer(Customer customer) {
		List<Customer> customerList = new ArrayList<Customer>();
		customerRepository.save(customer);
		customerList =customerRepository.findAll();
		return customerList;
	}

	@Override
	public List<Customer> dummyCustomers() {
		List<Customer>customers = new ArrayList<Customer>();
		
		Customer customer1 = new Customer();
		
		customer1.setName("Home Depot--LawrenceVille1 Site");
		customer1.setPhone("231-342-1234");
		customers.add(customer1);
		
		
		return customers;
	}

}
