package com.custanalytics.demo.serviceorder.service;

import org.springframework.stereotype.Service;

import com.custanalytics.demo.serviceorder.model.Customer;

import java.util.List;



public interface CustomerService {
	
	public List<Customer> getlAllCustomers();
	
	public Customer getCustomer(Long customerId);
	
	public List<Customer>  findAssignedCustomers(Long technicianId);
	
	public void saveCustomer(Customer customer);
	
	public List<Customer> updateCustomer(Customer customer);
	
	public List<Customer> dummyCustomers();
}