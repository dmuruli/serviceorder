package com.custanalytics.demo.serviceorder.model;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMER")
public class Customer {
	
	/**
	 * CREATE TABLE IF NOT EXISTS `CUSTOMER` (
  `CUSTOMER_ID` bigint NOT NULL ,
  `CUSTOMER_NAME` varchar(150)  NULL,
  `FIRST_NAME` varchar(150)  NULL,
  `LAST_NAME` varchar(150)  NULL,
  `NOTES` varchar(250)  NULL,
  `CUSTOMER_PHONE` varchar(20) NOT NULL ,
  `CUSTOMER_TYPE` varchar(20) NOT NULL ,
  PRIMARY KEY (CUSTOMER_ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
	 */
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private Long customerId;
	@Column(name = "CUSTOMER_NAME")
	private String name;
	@Column(name = "CUSTOMER_PHONE")
	private String phone;
	@Column(name = "FIRST_NAME")
	private String firstName;
	@Column(name = "LAST_NAME")
	private String lastName;
	@Column(name = "NOTES")
	private String notes;
	@Column(name = "CUSTOMER_TYPE")
	private String type;
	 @OneToMany(cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	  @JoinTable
	  (
	      name="CUSTOMER_ADDRESS",
	      joinColumns={ @JoinColumn(name="CUSTOMER_ID", referencedColumnName="CUSTOMER_ID" ) },
	      inverseJoinColumns={ @JoinColumn(name="ADDRESS_ID" , referencedColumnName="ADDRESS_ID") }
	  )
	private Set<Address> addressList;
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Set getAddressList() {
		return addressList;
	}
	public void setAddressList(Set addressList) {
		this.addressList = addressList;
	}
	
	
	
}
