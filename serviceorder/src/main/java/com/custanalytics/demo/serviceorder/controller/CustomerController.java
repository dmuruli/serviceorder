package com.custanalytics.demo.serviceorder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.custanalytics.demo.serviceorder.model.Customer;
import com.custanalytics.demo.serviceorder.service.CustomerService;

@Controller
public class CustomerController {
	@Autowired
	CustomerService customerService;

	@RequestMapping(value = "/customerList")
	public ModelAndView initCustomerList() {
		ModelAndView mav = new ModelAndView("customerList");
		List<Customer>customers =customerService.getlAllCustomers();
		mav.addObject("customers", customers);
		return mav;
	}
	@RequestMapping(value = "/customerDetail")
	public ModelAndView getCustomerDetail(
			@ModelAttribute("customerId") Long customerId) {
		ModelAndView mav = new ModelAndView("customerDetail");
		Customer customer =customerService.getCustomer(customerId);
		mav.addObject(customer);
		return mav;
	}
	@RequestMapping(value = "/createCustomer")
	public ModelAndView saveCustomer(
			@ModelAttribute("customerId")Customer customer) {
		ModelAndView mav = new ModelAndView("customerList");
		List<Customer>customers =customerService.getlAllCustomers();
		
		//Create dummy list of customers
		customers=customerService.dummyCustomers();
		mav.addObject("customers", customers);
		return mav;
	}
	@RequestMapping(value = "/updateCustomer")
	public ModelAndView updateCustomer(
			@ModelAttribute("customerId")Customer customer) {
		ModelAndView mav = new ModelAndView("customerDetail");
		
		return mav;
	}
}
