package com.custanalytics.serviceorder.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@EnableWebMvc
@EnableJpaRepositories(basePackages = { "com.custanalytics.demo.serviceorder.dao" })
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.custanalytics.demo.serviceorder.service,com.custanalytics.demo.serviceorder.controller" })
public class AppConfig extends WebMvcConfigurerAdapter {

	@Bean
	public InternalResourceViewResolver getInternalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/pages/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").addResourceLocations("/css/");
		registry.addResourceHandler("/img/**").addResourceLocations("/img/");
		registry.addResourceHandler("/js/**").addResourceLocations("/js/");
		registry.addResourceHandler("/fonts/**")
				.addResourceLocations("/fonts/");
	}

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/servicework");
		dataSource.setUsername("service_or_app");
		dataSource.setPassword("passw0!?rd");

		return dataSource;
	}

	private Properties getHibernateProperties() {
		Properties prop = new Properties();

		prop.put("hibernate.format_sql", "true");
		prop.put("hibernate.show_sql", "true");
		prop.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

		prop.put("hibernate.c3p0.min_size", "5");
		prop.put("hibernate.c3p0.max_size", "10");
		prop.put("hibernate.c3p0.max_statements", "50");
		prop.put("hibernate.c3p0.idle_test_period", "3000");

		return prop;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(getDataSource());
		em.setPackagesToScan(new String[] { "com.custanalytics.demo.serviceorder.model" });

		JpaVendorAdapter vendorAdaptor = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdaptor);
		em.setJpaProperties(getHibernateProperties());

		return em;
	}

	@Bean
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory emf) {
		JpaTransactionManager txMgr = new JpaTransactionManager();
		txMgr.setEntityManagerFactory(emf);
		return txMgr;
	}

	@Bean(name = "persistenceExceptionTranslationPostProcessor")
	public PersistenceExceptionTranslationPostProcessor getExceptionProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	

}
