package com.custanalytics.serviceorder.dao.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.util.Assert;

import com.custanalytics.demo.serviceorder.model.AppUser;
import com.custanalytics.demo.serviceorder.repository.AccountDao;
import com.custanalytics.serviceorder.config.TestAppConfig;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestAppConfig.class }) // using test app to remove servlet dependecies.
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:accountTestData.xml")
public class TestAccountDao {

	@Autowired
	private AccountDao accountRepository;
	public void testGetAppUser() {
	}
	
	@Test
	public void testFindAllAppUsers() {
		List<AppUser> users = accountRepository.findAll();
		System.out.println("Found " + users.size() + " users");
	}
	@Test
	public void testSaveAppUser() {
		AppUser jane = accountRepository.login("janedoe@testdata.com", "Foo");
		Assert.notNull(jane);
		Assert.isTrue(jane.getPassword().equalsIgnoreCase("Foo"));
	}

}
