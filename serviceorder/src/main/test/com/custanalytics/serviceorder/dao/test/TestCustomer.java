package com.custanalytics.serviceorder.dao.test;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.junit.Assert;

import com.custanalytics.demo.serviceorder.model.Address;
import com.custanalytics.demo.serviceorder.model.Customer;
import com.custanalytics.demo.serviceorder.repository.CustomerRepository;
import com.custanalytics.serviceorder.config.TestAppConfig;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestAppConfig.class }) // using test app to remove servlet dependecies.
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:customerTestData.xml")
public class TestCustomer {
	@Autowired
	private CustomerRepository customerRepository;	
	@Test
	@Transactional
	public void testFindAllCustomers(){
		List<Customer>customers = customerRepository.findAll();
		System.out.println("No. of customers: " + customers.size());
		Collection<Address>addressList =customers.get(0).getAddressList();
		System.out.println("Size of addressList 1: " + addressList.size());
		Collection<Address>addressList2 =customers.get(1).getAddressList();
	}
	@Test
	@Transactional
	public void testFindCustomerById(){
		Long customerId = 1L;
		Customer customer = customerRepository.findOne(customerId);
		Assert.assertTrue("Customer name is not expected name: FRY'S ELECTRONICS", customer.getName().equalsIgnoreCase("FRY'S ELECTRONICS"));
	}

}
