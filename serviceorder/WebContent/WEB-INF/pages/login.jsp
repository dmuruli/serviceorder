
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<!-- Title and other stuffs -->
<title>Login-Cust Analytics Demo</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<!-- Stylesheets -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href="css/style.css" rel="stylesheet">

<script src="js/respond.min.js"></script>
<!-- Move this into a css file. -->
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>
<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <![endif]-->

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon/favicon.png">
</head>

<body>

	<!-- Form area -->
	<div class="admin-form">
		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<spring:hasBindErrors name="user">
											
							<div class="error">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
										<li>${error.defaultMessage}</li>
									</c:forEach>
								</ul>
							</div>
						
					</spring:hasBindErrors>
					<!-- Widget starts -->
					<div class="widget worange">
						<!-- Widget head -->
						<div class="widget-head">
							<i class="fa fa-lock"></i>Login
						</div>

						<div class="widget-content">
						<form:errors path="*" cssClass="error" element="div" />
							<div class="padd">
								<!-- Login form -->
								<form:form class="form-horizontal" role="form"
									commandName="user" action="login.html" method="post">
									<!-- Email -->
									<div class="form-group">
										<label class="control-label col-lg-3" for="inputEmail">Email</label>
										<div class="col-lg-9">
											<form:input class="form-control" placeholder="Email"
												path="email" type="email" autofocus="true" />
											<form:errors path="email" cssClass="error" />
										</div>
									</div>
									<!-- Password -->
									<div class="form-group">
										<label class="control-label col-lg-3" for="inputPassword">Password</label>
										<div class="col-lg-9">
											<form:input class="form-control" placeholder="Password"
												path="password" type="password" value="" />
											<form:errors path="password" cssClass="error" />
										</div>
									</div>
									<!-- Remember me checkbox and sign in button -->
									<div class="form-group">
										<div class="col-lg-9 col-lg-offset-3">
											<div class="checkbox">
												<label> <input type="checkbox"> Remember me
												</label>
											</div>
										</div>
									</div>
									<div class="col-lg-9 col-lg-offset-3">
										<button type="submit" class="btn btn-info btn-sm">Sign
											in</button>
										<button type="reset" class="btn btn-default btn-sm">Reset</button>
									</div>
									<br />
								</form:form>

							</div>
						</div>

						<div class="widget-foot">
							Not Registred? <a href="#">Register here</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- JS -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>